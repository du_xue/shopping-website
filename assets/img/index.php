<?php
/**
 * Main entry point of the system
 *
 * @author Lindsay Marshall <lindsay.marshall@ncl.ac.uk>
 * @copyright 2012-2013 Newcastle University
 *
 */
    require_once('lib/startup.php');

    Context::initialise(FALSE);

    
    $action = strtolower(Context::getpar('_action', 'home'));
    $rest = Context::getpar('_rest', '');
   // Goods::addGoods();
    

    $vals = array(
	'page'          => $action,
	'subd'          => Local::$subd,
	'assets'        => Local::$assets,
	'shared'        => Local::$shared,
	'website'       => Context::$site,
	'admin'         => Context::sessioncheck('admin', FALSE),        
        'loggedin'        =>TRUE
    );
    
    $tpl = '';
   
    switch ($action)
    {
        /**
         * AJAX
         */
    case"gethint":
        $p = Context::getpar('checkuser','');
       // $response = Register::suggestUserName($p);
       //echo $response;                     // give a suggestting a name
        $responsecheck = Register::checkUser($p);
        echo $responsecheck;
        
        break;
    case"checklogin":
        $p= Context::getpar('loginuser','');
        $ack=  Login::checkLogin($p);
        echo $ack;  
        /**
         * normal jumping web page
         */
        break;
    case 'index.php':
        $tpl = 'index.twig';  
        echo Context::render($tpl, $vals);
        break;
     case 'logout' :
        $tpl = 'index.twig';
        $vals['loggedin']=TRUE; 
        $vals['loggeduser']=null;
        session_destroy();
        
        echo Context::render($tpl, $vals);
        break;
   
    case 'home' :
        session_start();
        $tpl = 'index.twig';
        $ackout=Context::getpar('ackout','');
        if($ackout=='logout'){
            $vals['loggedin']=TRUE; 
            $vals['loggeduser']=null;
            session_destroy();
        }
        $ack=Session::openUserSession($_POST);
        if($_SESSION['loginuser']!=null){
        $vals['loggedin']=FALSE; 
        $vals['loggeduser']=$_SESSION['loginuser'];
         
        }
        echo Context::render($tpl, $vals);
        break;

    case 'info' :
	phpinfo();
	exit;
        
    case 'test':
        $tpl = 'test.twig';
        echo Context::render($tpl, $vals);
        break;
    
    case 'register':
        $tpl = 'register.twig';
        echo Context::render($tpl, $vals);
        break;
    
    case 'newuser':
        $responsecheck = Register::checkUser($_POST['user']);
        if($responsecheck == 'This name has been used'){
             $vals['p']= $responsecheck;
             $tpl='register.twig';
        }
        else{
        $tpl='newuser.twig';
        $newuser = Register::addNewUser($_POST);
        $vals['ack'] = $newuser;
        }
        echo Context::render($tpl, $vals);
        break;
    
    case 'login':
        $tpl = 'login.twig'; 
        echo Context::render($tpl, $vals);
        break;
    
    case 'shopping':
        $ack=Goods::findGoods();
        
        $vals['showallgoods']=$ack;
        $tpl='shopping.twig';
         if($_SESSION['loginuser']!=null){
        $vals['loggedin']=FALSE; 
        $vals['loggeduser']=$_SESSION['loginuser'];
        }
        echo Context::render($tpl, $vals);
        break;
        
    case 'addcart':
        $ack=Goods::findGoods();
        $vals['showallgoods']=$ack;
        $tpl='shopping.twig';
        $p=  Cart::addCart($_POST);
        $vals['totalprice']=$p;
        //echo $_POST['choice'];
        //echo $_POST['goodsid'];
        echo Context::render($tpl, $vals);
        break;
       
    default:
        echo Context::render($tpl, $vals);
	break;
    }
    //echo Context::render($tpl, $vals);
?>
