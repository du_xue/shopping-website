<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of register
 *
 * @author Du Xue
 */
class Register {
 /*
  * add new user
  */   
public function addNewUser($pa) {
    
    $bean = R::dispense('userinfo');
    $bean->user = $pa['user'];
    $bean->email = $pa['email'];
    $bean->password = $pa['password1'];
    R::store($bean);    
    return Success;
    }
 /*
  * suggest a username
  */   
public function suggestUserName($q){
$a[]="Anna2013";
$a[]="Brittany2013";
$a[]="Cinderella2013";
$a[]="Diana2013";
$a[]="Eva2013";
$a[]="Fiona2013";
$a[]="Gunda2013";
$a[]="Hege2013";
$a[]="Inga2013";
$a[]="Johanna2013";
$a[]="Kitty2013";
$a[]="Linda2013";
$a[]="Nina2013";
$a[]="Ophelia2013";
$a[]="Petunia12013";
$a[]="Amanda2013";
$a[]="Raquel2013";
$a[]="Cindy2013";
$a[]="Doris2013";
$a[]="Eve2013";
$a[]="Evita2013";
$a[]="Sunniva2013";
$a[]="Tove2013";
$a[]="Unni12013";
$a[]="Violet2013";
$a[]="Liza2013";
$a[]="Elizabeth2013";
$a[]="Ellen2013";
$a[]="Wenche2013";
$a[]="Vicky2013";

//get the q parameter from URL
//$q=$_GET["q"];
  
//lookup all hints from array if length of q>0
if (strlen($q) > 0)
  {

  $hint="";
  for($i=0; $i<count($a); $i++)
    {
    if (strtolower($q)==strtolower(substr($a[$i],0,strlen($q))))
      {
      if ($hint=="")
        {
        $hint=$a[$i];
        }
      else
        {
        $hint=$hint." , ".$a[$i];
        }
      }
    }
  }

// Set output to "no suggestion" if no hint were found
// or to the correct values
if ($hint == "")
  {
  return "(".$q."2013".")";
  }
else 
  {
  $response="(".$hint.")";
  }

//output the response
return $response;
//put your code here
}
/*
 * check the username is used or not
 */
public function checkUser($param){
    
   $user = R::find('userinfo', ' user = :username ', array( ':username'=>$param ));
   
   if($user != null)
       return 'This name has been used';
   else
       return 'You can use this name';    
}
}

?>
