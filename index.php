<?php

/**
 * Main entry point of the system
 *
 * @author Lindsay Marshall <lindsay.marshall@ncl.ac.uk>
 * @copyright 2012-2013 Newcastle University
 *
 */
require_once('lib/startup.php');

Context::initialise(FALSE);


$action = strtolower(Context::getpar('_action', 'home'));
$rest = Context::getpar('_rest', '');
//Goods::addGoods();


$vals = array(
    'page' => $action,
    'subd' => Local::$subd,
    'assets' => Local::$assets,
    'shared' => Local::$shared,
    'website' => Context::$site,
    'admin' => Context::sessioncheck('admin', FALSE),
    'loggedin' => TRUE
);

$tpl = '';

switch ($action) {
     /**
     * Simple AJAX, return the hint
     */
    case"gethint":
        $p = Context::getpar('checkuser', '');
        $response = Register::suggestUserName($p);
        echo $response;                     // give a suggestting a name
        $responsecheck = Register::checkUser($p);
        echo $responsecheck;
        break;
    
     /**
     * Check the user is logged in or not
     */
    case"checklogin":
        $p = Context::getpar('loginuser', '');
        
        $ack = Login::checkLogin($p);
        echo $ack;
     /**
     * normal jumping web page
     */
        break;
    /**
     * Default homepage
     */
    case 'index.php':
        $tpl = 'index.twig';
        echo Context::render($tpl, $vals);
        break;
    
    /**
    * The user loge out, destory the session
    */
    case 'logout' :
        session_start();
        $tpl = 'index.twig';
        $vals['loggedin'] = TRUE;
        $vals['loggeduser'] = null;
        session_destroy();//destory the session

        echo Context::render($tpl, $vals);
        break;
    /**
     * back to home page, show the user logged or not
     */
    case 'home' :
        session_start();
       
        $tpl = 'index.twig';
        $ackout = Context::getpar('ackout', '');

        if ($ackout == 'logout') {
            $vals['loggedin'] = TRUE;   //show the login bar in the twig again
            $vals['loggeduser'] = null;
            session_destroy();
        }

        $ack = Session::openUserSession($_POST);
        if ($_SESSION['loginuser'] != null) {
            $vals['loggedin'] = FALSE;   //the user is still logged, do not show the login bar in twig
            $vals['loggeduser'] = $_SESSION['loginuser'];//get the username from session
        }
        echo Context::render($tpl, $vals);
        break;

    case 'info' :
        phpinfo();
        exit;
        
    /**
    * Go to the register page
    */
    case 'register':
        $tpl = 'register.twig';
        echo Context::render($tpl, $vals);
        break;
    /**
    * go to shooping page
    * check the user logged or not
    */
    case 'shopping':

        if ($_SESSION['loginuser'] != null) {//the user muset logged in 
            $totalgoods = Goods::findGoods();
            $vals['totalgoods'] = $totalgoods;
            $tpl = 'shoppingcart.twig';
            $vals['loggeduser'] = $_SESSION['loginuser'];
        } else {
            $vals['warn'] = True;
            $tpl = 'index.twig';
            $vals['pleasesignup'] = "Please register first!";
        }
        echo Context::render($tpl, $vals);
        break;
        /**
         * add the new user in database
         */
        
        case 'newuser':
        $responsecheck = Register::checkUser($_POST['user']);
        if($responsecheck == 'This name has been used'){
             $vals['p']= $responsecheck;
             $tpl='register.twig';
        }
        else{
        $tpl='newuser.twig';
        $newuser = Register::addNewUser($_POST);
        $vals['ack'] = $newuser;
        }
        echo Context::render($tpl, $vals);
        break;
    
    /**
    * Go to the checkout page, show totalmoney
    */
    case 'checkout':
        $tpl = 'checkout.twig';
        $vals['totalmoney'] = Context::getpar('total-hidden-charges', '');//pass the total money
        echo Context::render($tpl, $vals);
        break;
   /**
    * Default
    */
    default:
        echo Context::render($tpl, $vals);
        break;
}
//echo Context::render($tpl, $vals);
?>
