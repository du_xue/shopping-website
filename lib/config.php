<?php
    
    define('DBHOST', 'localhost');
    define('DB', 'fvmarket');
    define('DBUSER', 'root');
    define('DBPW', '');
    define('SUBDIRECTORY', '/webbase');
    define('SYSADMIN', 'email@address');
    define('SITE', 'webbase');
    define('MAILHOST', 'mailhost.co.uk');
    define('COMMON', SUBDIRECTORY);
    define('ASSETS', SUBDIRECTORY.'/assets');
    define('SHARED', SUBDIRECTORY);
?>
